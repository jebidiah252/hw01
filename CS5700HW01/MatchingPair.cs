﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS5700HW01
{
    public class MatchingPair
    {
        public int firstObjectID { get; set; }

        public int secondObjectID { get; set; }
    }
}
