﻿using System;
using CS5700HW01.IO;
using CS5700HW01.Matching;

namespace CS5700HW01
{
    class Program
    {
        static void Main(string[] args)
        {
            
            MatchingProgram mainProject = new MatchingProgram();
            int input = 0;
            while(input != 6)
            {
                writeMenu();
                string consoleInput = Console.ReadLine();
                input = Convert.ToInt32(consoleInput);
                switch (input)
                {
                    case 1:
                        mainProject.runProgram("PersonTestSet_11.xml");
                        break;
                    case 2:
                        mainProject.runProgram("PersonTestSet_12.xml");
                        break;
                    case 3:
                        mainProject.runProgram("PersonTestSet_13.xml");
                        break;
                    case 4:
                        mainProject.runProgram("PersonTestSet_14.xml");
                        break;
                    case 5:
                        mainProject.runProgram("PersonTestSet_15.xml");
                        break;
                    case 6:
                        Console.WriteLine("Thanks for your participation!");
                        break;
                    default:
                        Console.WriteLine("That is not a valid option");
                        break;
                }
            }  
        }

        public static void writeMenu()
        {
            Console.WriteLine("Welcome to the first assignment!");
            Console.WriteLine("Here are your options");
            Console.WriteLine("1. Read and run data from PersonTestSet_11.xml");
            Console.WriteLine("2. Read and run data from PersonTestSet_12.xml");
            Console.WriteLine("3. Read and run data from PersonTestSet_13.xml");
            Console.WriteLine("4. Read and run data from PersonTestSet_14.xml");
            Console.WriteLine("5. Read and run data from PersonTestSet_15.xml");
            Console.WriteLine("6. Exit the program");
        }
    }
}
