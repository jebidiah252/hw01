﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS5700HW01.Matching
{
    public abstract class Matching
    {
        public abstract bool testMatchOne(Person firstPerson, Person secondPerson);
        public abstract bool testMatchTwo(Person firstPerson, Person secondPerson);
        public abstract bool testMatchThree(Person firstPerson, Person secondPerson);
        public abstract bool testMatchFour(Person firstPerson, Person secondPerson);
        public abstract bool testMatchFive(Person firstPerson, Person secondPerson);
    }
}
