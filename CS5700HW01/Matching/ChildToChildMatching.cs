﻿namespace CS5700HW01.Matching
{
    public class ChildToChildMatching : Matching
    {
        public override bool testMatchFive(Person firstPerson, Person secondPerson)
        {
            if (firstPerson == secondPerson)
            {
                return true;
            }
            return false;
        }

        public override bool testMatchFour(Person firstPerson, Person secondPerson)
        {
            if (((firstPerson.BirthDay != 0 && secondPerson.BirthDay != 0)
                && (firstPerson.BirthMonth != 0 && secondPerson.BirthMonth != 0)
                && (firstPerson.BirthYear != 0 && secondPerson.BirthYear != 0))
                && (firstPerson.SocialSecurityNumber != "" && secondPerson.SocialSecurityNumber != ""))
            {
                if (((firstPerson.FirstName == secondPerson.FirstName)
                    && (firstPerson.MiddleName == secondPerson.MiddleName)
                    && (firstPerson.LastName == secondPerson.MiddleName))
                    && (firstPerson.SocialSecurityNumber == secondPerson.SocialSecurityNumber))
                {
                    return true;
                }
            }
            return false;
        }

        public override bool testMatchOne(Person firstPerson, Person secondPerson)
        {
            if (((firstPerson.StateFileNumber != "" && secondPerson.StateFileNumber != "")
                || (firstPerson.SocialSecurityNumber != "" && secondPerson.SocialSecurityNumber != ""))
                && ((firstPerson.BirthYear != 0 && secondPerson.BirthYear != 0)
                    && (firstPerson.BirthMonth != 0 && secondPerson.BirthMonth != 0)
                    && (firstPerson.BirthDay != 0 && secondPerson.BirthDay != 0)))
            {
                if ((firstPerson.BirthDay == secondPerson.BirthDay
                    && firstPerson.BirthMonth == secondPerson.BirthMonth
                    && firstPerson.BirthYear == secondPerson.BirthYear)
                    && ((firstPerson.StateFileNumber == secondPerson.StateFileNumber)
                        || (firstPerson.SocialSecurityNumber == secondPerson.SocialSecurityNumber)))
                {
                    return true;
                }
            }
            return false;
        }

        public override bool testMatchThree(Person firstPerson, Person secondPerson)
        {
            if (((firstPerson.FirstName != "" && secondPerson.FirstName != "")
                && (firstPerson.MiddleName != "" && secondPerson.MiddleName != "")
                && (firstPerson.LastName != "" && secondPerson.LastName != ""))
                && (firstPerson.Gender != "" && secondPerson.Gender != "")
                && ((firstPerson.BirthDay != 0 && secondPerson.BirthDay != 0)
                    && (firstPerson.BirthMonth != 0 && secondPerson.BirthMonth != 0)
                    && (firstPerson.BirthYear != 0 && secondPerson.BirthYear != 0)))
            {
                if (((firstPerson.FirstName == secondPerson.FirstName)
                    && (firstPerson.MiddleName == secondPerson.MiddleName)
                    && (firstPerson.LastName == secondPerson.LastName))
                    && (firstPerson.Gender == secondPerson.Gender)
                    && ((firstPerson.BirthDay == secondPerson.BirthDay)
                        && (firstPerson.BirthMonth == secondPerson.BirthMonth)
                        && (firstPerson.BirthYear == secondPerson.BirthYear)))
                {
                    return true;
                }
            }
            return false;
        }

        public override bool testMatchTwo(Person firstPerson, Person secondPerson)
        {
            if (((firstPerson.StateFileNumber != "" && secondPerson.StateFileNumber != "")
               || (firstPerson.SocialSecurityNumber != "" && secondPerson.SocialSecurityNumber != ""))
               && (((firstPerson.FirstName != null && secondPerson.FirstName != null) && (firstPerson.MiddleName != null && secondPerson.MiddleName != null))
                    || ((firstPerson.MiddleName != null && secondPerson.MiddleName != null) && (firstPerson.LastName != null && secondPerson.LastName != null))
                    || ((firstPerson.LastName != null && secondPerson.LastName != null) && (firstPerson.FirstName != null && secondPerson.FirstName != null))))
            {
                if (((firstPerson.StateFileNumber == secondPerson.StateFileNumber)
                        || (firstPerson.SocialSecurityNumber == secondPerson.SocialSecurityNumber))
                        && ((firstPerson.FirstName == secondPerson.FirstName && firstPerson.MiddleName == secondPerson.MiddleName)
                            || (firstPerson.FirstName == secondPerson.FirstName && firstPerson.LastName == secondPerson.LastName)
                            || (firstPerson.MiddleName == secondPerson.MiddleName && firstPerson.LastName == secondPerson.LastName)))
                {
                    return true;
                }

            }
            return false;
        }
    }
}
