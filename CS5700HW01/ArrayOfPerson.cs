﻿using System.Collections.Generic;
using System.Collections;
using CS5700HW01.IO;

namespace CS5700HW01
{
    public class ArrayOfPerson : IEnumerable<Person>
    {
        private List<Person> persons = new List<Person>();

        public ImporterExporter storage { get; set; }

        public void Add(Person person)
        {
            if(person != null)
            {
                persons.Add(person);
            }
        }

        public void Load(string filename)
        {
            if(!string.IsNullOrWhiteSpace(filename) && storage != null)
            {
                persons = storage.Load(filename);
            }
        }

        public void Save(string filename)
        {
            if (!string.IsNullOrWhiteSpace(filename) && storage != null)
            {
                storage.Save(filename, persons);
            }
        }

        public IEnumerator<Person> GetEnumerator()
        {
            return persons.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new PersonEnum(persons.ToArray());
        }
    }

    public class PersonEnum : IEnumerator
    {
        private readonly Person[] persons;
        private int currentPos = -1;

        public PersonEnum(Person[] personsPassed)
        {
            persons = personsPassed;
        }

        public bool MoveNext()
        {
            return (++currentPos < persons.Length);
        }

        public void Reset()
        {
            currentPos = -1;
        }

        object IEnumerator.Current { get { return Current; } }

        public Person Current
        {
            get
            {
                return (currentPos >= 0 && currentPos < persons.Length) ? persons[currentPos] : null;
            }
        }
    }
}
