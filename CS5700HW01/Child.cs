﻿using System.Runtime.Serialization;

namespace CS5700HW01
{
    [DataContract]
    public class Child : Person
    {
        [DataMember]
        public string NewbornScreeningNumber { get; set; }

        [DataMember]
        public string IsPartOfMultipleBirth { get; set; }

        [DataMember]
        public int BirthOrder { get; set; }

        [DataMember]
        public string BirthCounty { get; set; }

        [DataMember]
        public string MotherFirstName { get; set; }

        [DataMember]
        public string MotherLastName { get; set; }

        [DataMember]
        public string MotherMiddleName { get; set; }
    }
}
