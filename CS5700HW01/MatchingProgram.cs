﻿using System;
using CS5700HW01.IO;
using CS5700HW01.Matching;

namespace CS5700HW01
{
    public class MatchingProgram
    {
        public void runProgram(string filename)
        {
            ArrayOfPerson people = new ArrayOfPerson();
            people.storage = new XMLImporterExporter();
            people.Load(filename);

            foreach(var person in people)
            {
                InputCleaner cleaner = new InputCleaner();
                if(person.GetType() == typeof(Child))
                {
                    Child temp = new Child();
                    temp = (Child)person;
                    cleaner.cleanChild(ref temp);
                }
                else if(person.GetType() == typeof(Adult))
                {
                    Adult temp = new Adult();
                    temp = (Adult)person;
                    cleaner.cleanAdult(ref temp);
                }
            }

            Console.WriteLine("These are the matches");

            foreach(var person in people)
            {
                AdultToAdultMatching adultMatcher = new AdultToAdultMatching();
                ChildToAdultMatching mixingMatcher = new ChildToAdultMatching();
                ChildToChildMatching childMatcher = new ChildToChildMatching();

                foreach(var thisGuy in people)
                {
                    if(person.GetType() == typeof(Adult) && (thisGuy.GetType() == typeof(Adult)))
                    {
                        if(adultMatcher.testMatchOne(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (adultMatcher.testMatchTwo(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (adultMatcher.testMatchThree(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (adultMatcher.testMatchFour(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (adultMatcher.testMatchOne(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                    }
                    else if(person.GetType() == typeof(Adult) && thisGuy.GetType() == typeof(Child))
                    {
                        if (mixingMatcher.testMatchOne(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (mixingMatcher.testMatchTwo(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (mixingMatcher.testMatchThree(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (mixingMatcher.testMatchFour(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (mixingMatcher.testMatchOne(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                    }
                    else if (person.GetType() == typeof(Child) && (thisGuy.GetType() == typeof(Child)))
                    {
                        if (childMatcher.testMatchOne(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (childMatcher.testMatchTwo(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (childMatcher.testMatchThree(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (childMatcher.testMatchFour(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                        else if (childMatcher.testMatchOne(person, thisGuy) && person.ObjectId != thisGuy.ObjectId)
                        {
                            Console.WriteLine("{0}, {1}", person.ObjectId, thisGuy.ObjectId);
                            continue;
                        }
                    }
                }
            }
        }
    }
}
