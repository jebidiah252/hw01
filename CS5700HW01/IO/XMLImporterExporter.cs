﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CS5700HW01.IO
{
    public class XMLImporterExporter : ImporterExporter
    {
        public override List<Person> Load(string filename)
        {
            List<Person> persons = null;
            XmlSerializer serializer = new XmlSerializer(typeof(List<Person>), new Type[] { typeof(Person), typeof(Child), typeof(Adult) });

            if(OpenReader(filename))
            {
                try
                {
                    persons = serializer.Deserialize(Reader) as List<Person>;
                }
                catch(Exception e)
                {
                    Console.Write(e);
                }
                Reader.Close();
            }

            return persons;
        }

        public override void Save(string filename, List<Person> persons)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Person>), new Type[] { typeof(Person), typeof(Child), typeof(Adult) });

            if (OpenWriter(filename))
            {
                serializer.Serialize(Writer, persons);
                Writer.Close();
            }
        }
    }
}
