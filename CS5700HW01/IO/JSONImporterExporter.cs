﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;

namespace CS5700HW01.IO
{
    public class JSONImporterExporter : ImporterExporter
    {
        public override List<Person> Load(string filename)
        {
            List<Person> persons = null;
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<Person>), new Type[] { typeof(Person), typeof(Adult), typeof(Child) });

            if(OpenReader(filename))
            {
                try
                {
                    persons = serializer.ReadObject(Reader.BaseStream) as List<Person>;
                    Reader.Close();
                }
                catch(Exception e)
                {
                    Console.Write(e);
                }
            }

            return persons;
        }

        public override void Save(string filename, List<Person> persons)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<Person>), new Type[] { typeof(Person), typeof(Adult), typeof(Child) });

            if(OpenWriter(filename))
            {
                serializer.WriteObject(Writer.BaseStream, persons);
                Writer.Close();
            }
        }
    }
}
