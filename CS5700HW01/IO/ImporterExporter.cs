﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CS5700HW01.IO
{
    public abstract class ImporterExporter
    {
        protected StreamReader Reader { get; set; }

        protected StreamWriter Writer { get; set; }
        public abstract List<Person> Load(string filename);
        public abstract void Save(string filename, List<Person> persons);

        protected virtual bool OpenReader(string filename)
        {
            bool result = false;
            try
            {
                Reader = new StreamReader(filename);
                result = true;
            }
            catch(Exception e)
            {
                Console.WriteLine($"Cannot open input file {filename}, error = {e}");
            }

            return result;
        }

        protected virtual bool OpenWriter(string filename)
        {
            bool result = true;
            try
            {
                Writer = new StreamWriter(filename);
                result = true;
            }
            catch(Exception e)
            {
                Console.WriteLine($"Cannot open input file {filename}, error = {e}");
            }

            return result;
        }
    }
}
