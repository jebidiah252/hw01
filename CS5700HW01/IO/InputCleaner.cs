﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS5700HW01.IO
{
    public class InputCleaner
    {
        public void cleanChild(ref Child child)
        {
            if(child.BirthCounty == null)
            {
                child.BirthCounty = "";
            }
            if(child?.BirthDay == null)
            {
                child.BirthDay = 0;
            }
            if(child?.BirthMonth == null)
            {
                child.BirthMonth = 0;
            }
            if(child?.BirthYear == null)
            {
                child.BirthYear = 0;
            }
            if(child?.BirthOrder == null)
            {
                child.BirthOrder = -1;
            }
            if(child.FirstName == null)
            {
                child.FirstName = "";
            }
            if(child.Gender == null)
            {
                child.Gender = "";
            }
            if (child.IsPartOfMultipleBirth == null)
            {
                child.IsPartOfMultipleBirth = "";
            }
            if(child.LastName == null)
            {
                child.LastName = "";
            }
            if(child.MiddleName == null)
            {
                child.MiddleName = "";
            }
            if(child.MotherFirstName == null)
            {
                child.MotherFirstName = "";
            }
            if(child.MotherLastName == null)
            {
                child.MotherLastName = "";
            }
            if(child.MotherMiddleName == null)
            {
                child.MotherMiddleName = "";
            }
            if(child.NewbornScreeningNumber == null)
            {
                child.NewbornScreeningNumber = "";
            }
            if(child.SocialSecurityNumber == null)
            {
                child.SocialSecurityNumber = "";
            }
            if(child.StateFileNumber == null)
            {
                child.StateFileNumber = "";
            }
        }

        public void cleanAdult(ref Adult adult)
        {
            if(adult.StateFileNumber == null)
            {
                adult.StateFileNumber = "";
            }
            if(adult.SocialSecurityNumber == null)
            {
                adult.SocialSecurityNumber = "";
            }
            if(adult.FirstName == null)
            {
                adult.FirstName = "";
            }
            if(adult.MiddleName == null)
            {
                adult.MiddleName = "";
            }
            if(adult.LastName == null)
            {
                adult.LastName = "";
            }
            if(adult?.BirthYear == null)
            {
                adult.BirthYear = 0;
            }
            if(adult?.BirthMonth == null)
            {
                adult.BirthMonth = 0;
            }
            if(adult?.BirthDay == null)
            {
                adult.BirthDay = 0;
            }
            if(adult.Gender == null)
            {
                adult.Gender = "";
            }
            if(adult.Phone1 == null)
            {
                adult.Phone1 = "";
            }
            if(adult.Phone2 == null)
            {
                adult.Phone2 = "";
            }
        }
    }
}
